<?php
/*
Plugin Name: WSPF Park Page Generator
Description: Rewrites URLs like '/park/anderson_lake' to redirect to a fixed page that can contain a [parkid] shortcode.
             The new version also pulls the parkid for the submit report page.
Version: 0.2
*/

defined( 'ABSPATH' ) or die();

add_filter( 'query_vars', 'ppg_rewrite_add_var' );
function ppg_rewrite_add_var( $vars )
{
    $vars[] = 'parkid';
    return $vars;
}

function ppg_rewrite_activation()
{
    add_rewrite_rule('^park/([^/]+)/?','index.php?page_id=9903&parkid=$matches[1]','top');
    add_rewrite_rule('^submit-trip-report/([^/]+)/?','index.php?page_id=9898&parkid=$matches[1]','top');
    
    flush_rewrite_rules();
}

register_activation_hook( __FILE__, 'ppg_rewrite_activation' );
register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );

//[parkid]
function parkid_func( $atts ) {
	return get_query_var('parkid');
}
add_shortcode( 'parkid', 'parkid_func' );

?>