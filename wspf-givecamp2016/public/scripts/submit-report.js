'use strict';

// Initializes a report submitter
function ReportSubmitter() {
    this.checkSetup();

    this.getParkId();

    // Shortcuts to DOM Elements.
    this.messageForm = document.getElementById('message-form');
    this.messageInput = document.getElementById('message');
    this.ratingInput = document.getElementsByName('rating');
    this.imagePreview = document.getElementById('mediaPreview');
    this.tripDate = document.getElementById('trip-date');
    this.activities = document.getElementsByName('park-activities');
    this.submitButton = document.getElementById('submit');
    this.mediaCapture = document.getElementById('mediaCapture');
    this.userPic = document.getElementById('user-pic');
    this.userName = document.getElementById('user-name');
    this.signInButton = document.getElementById('sign-in');
    this.signOutButton = document.getElementById('sign-out');
    this.signInSnackbar = document.getElementById('must-signin-snackbar');
    this.notifyMessage = document.getElementById('notify_message');

    // Saves message on form submit.
    this.messageForm.addEventListener('submit', this.saveMessage.bind(this));
    this.signOutButton.addEventListener('click', this.signOut.bind(this));
    this.signInButton.addEventListener('click', this.signIn.bind(this));

    // Toggle for the button.
    var buttonTogglingHandler = this.toggleButton.bind(this);
    this.messageInput.addEventListener('keyup', buttonTogglingHandler);
    this.messageInput.addEventListener('change', buttonTogglingHandler);

    this.toggleButton(false);

    // Events for image upload.
    this.mediaCapture.addEventListener('change', this.readImage.bind(this));

    this.initFirebase();
}


ReportSubmitter.prototype.initFirebase = function() {
    // Shortcuts to Firebase SDK features.
    this.auth = firebase.auth();
    this.database = firebase.database();
    this.storage = firebase.storage();

    // Initiates Firebase auth and listen to auth state changes.
    this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};


// Checks that the Firebase SDK has been correctly setup and configured.
ReportSubmitter.prototype.checkSetup = function() {
    if (!window.firebase || !(firebase.app instanceof Function) || !window.config) {
        window.alert('You have not configured and imported the Firebase SDK. ' +
            'Make sure you go through the codelab setup instructions.');
    } else if (config.storageBucket === '') {
        window.alert('Your Firebase Storage bucket has not been enabled. Sorry about that. This is ' +
            'actually a Firebase bug that occurs rarely. ' +
            'Please go and re-generate the Firebase initialisation snippet (step 4 of the codelab) ' +
            'and make sure the storageBucket attribute is not empty. ' +
            'You may also need to visit the Storage tab and paste the name of your bucket which is ' +
            'displayed there.');
    }
};

// Gets a parameter passed to the page
ReportSubmitter.prototype.getParameterByName = function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};

// Gets the parkId parameter passed to the page
ReportSubmitter.prototype.getParkId = function() {
    this.parkId = this.getParameterByName('parkid');
};

// Return an array of the selected opion values
// select is an HTML select element
ReportSubmitter.prototype.getSelectValues = function(select) {
    var result = [];
    if (select) {
        var opt;
        for (var i = 0, iLen = select.length; i < iLen; i++) {
            opt = select[i];
            if (opt && opt.checked) {
                result.push(opt.value);
            }
        }
    }
    return result;
};


/***********************************************************
 * Auth code
 ***********************************************************/

// Signs-in Report Submitter.
ReportSubmitter.prototype.signIn = function() {
    // Sign in Firebase using popup auth and Google as the identity provider.
    var provider = new firebase.auth.GoogleAuthProvider();
    this.auth.signInWithPopup(provider);
};

// Signs-out of Report Submitter.
ReportSubmitter.prototype.signOut = function() {
    // Sign out of Firebase.
    this.auth.signOut();
};

// Triggers when the auth state change for instance when the user signs-in or signs-out.
ReportSubmitter.prototype.onAuthStateChanged = function(user) {
    if (user) { // User is signed in!
        this.clearNotification();

        // Get profile pic and user's name from the Firebase user object.
        var profilePicUrl = user.photoURL;
        var userName = user.displayName;
        var userId = user.uid;

        // Set the user's profile pic and name.
        this.userPic.style.backgroundImage = 'url(' + profilePicUrl + ')';
        this.userName.textContent = 'Signed in as: ' + userName;
        this.userId = userId;

        // Show user's profile and sign-out button.
        this.userName.removeAttribute('hidden');
        this.userPic.removeAttribute('hidden');
        this.signOutButton.removeAttribute('hidden');

        // Hide sign-in button.
        this.signInButton.setAttribute('hidden', 'true');

        // Load message ref
        this.setupMessageRef();
    } else { // User is signed out!
        // Hide user's profile and sign-out button.
        this.userName.setAttribute('hidden', 'true');
        this.userPic.setAttribute('hidden', 'true');
        this.signOutButton.setAttribute('hidden', 'true');

        // Show sign-in button.
        this.signInButton.removeAttribute('hidden');

        this.resetForm();
    }
};

// Returns true if user is signed-in. Otherwise false and displays a message.
ReportSubmitter.prototype.checkSignedInWithMessage = function() {
    // Return true if the user is signed in Firebase
    if (this.auth.currentUser) {
        return true;
    }

    this.displayNotification("Please sign in first!", ReportSubmitter.MESSAGE_TYPE.ERROR);
    // Display a message to the user using a Toast.
    // var data = {
    //   message: 'You must sign-in first',
    //   timeout: 2000
    // };
    // this.signInSnackbar.MaterialSnackbar.showSnackbar(data);
    // return false;
};

// END Auth Code



/***********************************************************
 * Submit Trip Report Code
 ***********************************************************/

// Captures the current contents of the form
ReportSubmitter.prototype.captureContents = function () {
    this.tripTime = Date.parse(this.tripDate.value);
    if (isNaN(this.tripTime)) {
        this.tripTime = new Date().getTime();
    }

    var selectedRating = document.querySelector('input[name = "rating"]:checked');
    this.starRating = 4;
    if (selectedRating) {
        this.starRating = selectedRating.value;
    }

    this.reportContent = this.messageInput.value;

    this.parkActivities = this.getSelectValues(this.activities);

    var otherActivitiesBox = document.getElementById('other-activities');
    if (otherActivitiesBox) {
        var otherActivities = otherActivitiesBox.value;
        if (otherActivities && otherActivities.trim()) {
            var moreActivities = otherActivities.split(',').clean();
            for (var i = 0, iLen = moreActivities.length; i < iLen; i++) {
                var activityName = moreActivities[i].trim();
                if (activityName) {
                    this.parkActivities.push(activityName);
                }
            }
        }
    }
};

// Load a reference to the messages database
ReportSubmitter.prototype.setupMessageRef = function() {
    if (this.parkId) {
        // Reference to the /messages/ database path.
        this.messagesRef = this.database.ref('new_posts/' + this.parkId);
        // Make sure we remove all previous listeners.
        this.messagesRef.off();
    }
    else {
        this.displayNotification("Don't have a park ID!", ReportSubmitter.MESSAGE_TYPE.ERROR);
    }
};

// Verifies if a selected file is valid for submission
ReportSubmitter.prototype.readImage = function() {
    var file = event.target.files[0];
    // Clear the selection in the file picker input.
    // this.mediaCapture.reset();
    this.mediaCapture.value == '';

    // Check if the file is an image.
    if (!file.type.match('image.*')) {
        this.displayNotification('You can only submit images!', ReportSubmitter.MESSAGE_TYPE.WARNING);
        return;
    }
    else if (file.size > 5242880) {
        this.displayNotification('Please select an image smaller than 5 MB!', ReportSubmitter.MESSAGE_TYPE.WARNING);
        return;
    }
    else {
        this.imageFile = file;
        var imagePreview = this.imagePreview;

        var reader = new FileReader();
        reader.onload = function(event) {
            imagePreview.setAttribute("src", event.target.result);
            imagePreview.removeAttribute("hidden");
        };

        reader.readAsDataURL(this.imageFile);
    }
};

// Saves a new message on the Firebase DB.
ReportSubmitter.prototype.saveMessage = function(e) {
    e.preventDefault();

    // Check that the user entered a message and is signed in.
    if (this.messageInput.value && this.checkSignedInWithMessage()) {
        var currentUser = this.auth.currentUser;
        this.captureContents();

        if (this.messagesRef) {
            if (this.parkId) {
                // Add a new message entry to the Firebase Database.
                this.messagesRef.push({
                    time: this.tripTime,
                    park_id: this.parkId,
                    user_id: currentUser.uid,
                    user_name: currentUser.displayName,
                    user_email: currentUser.email,
                    star_rating: this.starRating,
                    trip_report_content: this.reportContent,
                    image_url: '',
                    activities: this.parkActivities,
                    report_timestamp: new Date().getTime()
                }).then(function (data) {
                    if (this.imageFile) {
                        var submitReportPointer = this;
                        // Upload the image to Firebase Storage.
                        this.storage.ref(currentUser.uid + '/' + Date.now() + '/' + this.imageFile.name)
                            .put(this.imageFile, {contentType: this.imageFile.type})
                            .then(function (snapshot) {
                                // Get the file's Storage URI and update the chat message placeholder.
                                var filePath = snapshot.metadata.fullPath;
                                data.update({
                                    image_url: this.storage.ref(filePath).toString()
                                });
                                // Finalize
                                this.onSubmitComplete();
                            }.bind(this)).catch(function (error) {
                            console.error('There was an error uploading a file to Firebase Storage:', error);
                            submitReportPointer.displayNotification("Could not upload your image! Try a smaller image.",
                                                                    ReportSubmitter.MESSAGE_TYPE.ERROR);
                            // Finalize
                            submitReportPointer.onSubmitComplete();
                        });
                    }

                    this.displayNotification("Report submitted successfully! Taking you back to the park.", ReportSubmitter.MESSAGE_TYPE.GOOD);

                    // Clear message text field and SEND button state.
                    this.resetForm();

                    // Finalize
                    this.onSubmitComplete();

                    window.setTimeout(window.parent.location.href = "http://wspf.org/parks/" + parkId,3000);

                }.bind(this)).catch(function (error) {
                    console.error('Error writing new message to Firebase Database', error);
                    this.displayNotification("Can't submit message right now!", ReportSubmitter.MESSAGE_TYPE.ERROR);
                    this.resetForm();
                    this.onSubmitFailure();
                });
            }
            else {
                this.displayNotification("Can't submit message right now!", ReportSubmitter.MESSAGE_TYPE.ERROR);
                this.resetForm();
                this.onSubmitFailure();
            }
        }
        else {
            console.error("Don't have a reference to messageRef!");
            this.displayNotification("Can't submit message right now!", ReportSubmitter.MESSAGE_TYPE.ERROR);
            this.resetForm();
            this.onSubmitFailure();
        }
    }
};

// This function is called on successful report submit
// to perform any finalization activities.
ReportSubmitter.prototype.onSubmitComplete = function() {

};

// This function is called on failed report submit
// to perform any finalization activities.
ReportSubmitter.prototype.onSubmitFailure = function() {

};

// Resets the form.
ReportSubmitter.prototype.resetForm = function() {
    this.messageForm.reset();
    this.toggleButton(false);
    this.imageFile = undefined;
    this.imagePreview.setAttribute("hidden", "true");
};

// Enables or disables the submit button depending on the values of the input
// fields.
ReportSubmitter.prototype.toggleButton = function(state) {
    if (state) {
        if (state) {
            this.submitButton.removeAttribute('disabled');
        }
        else {
            this.submitButton.setAttribute('disabled', 'true');
        }
    }
    else {
        if (this.messageInput.value) {
            this.submitButton.removeAttribute('disabled');
        } else {
            this.submitButton.setAttribute('disabled', 'true');
        }
    }
};

// Displays a notification message
ReportSubmitter.prototype.displayNotification = function(text, type, timeout) {
    var notifyDiv = this.notifyMessage;

    notifyDiv.innerHTML = text;
    var messageClass = "notify-message-normal";
    switch(type) {
        case ReportSubmitter.MESSAGE_TYPE.ERROR:
            messageClass = "notify-message-error"
            break;
        case ReportSubmitter.MESSAGE_TYPE.WARNING:
            messageClass = "notify-message-warning";
            break;
        case ReportSubmitter.MESSAGE_TYPE.GOOD:
            messageClass = "notify-message-good";
            break;
        case ReportSubmitter.MESSAGE_TYPE.NORMAL:
        default:
            messageClass = "notify-message-normal";
            break;
    }
    notifyDiv.className = messageClass;

    if (!timeout) {
        timeout = 10000;
    }

    if (this.notifyTimer) {
        clearTimeout(this.notifyTimer);
    }
    this.notifyTimer = setTimeout(function() {
        notifyDiv.innerHTML = '';
    }, timeout);
};

// Clears the notification message
ReportSubmitter.prototype.clearNotification = function() {
    this.notifyMessage.innerHTML = '';
    if (this.notifyTimer) {
        clearTimeout(this.notifyTimer);
    }
};

// Template for messages.
ReportSubmitter.MESSAGE_TEMPLATE =
    '<div class="message-container">' +
    '<div class="spacing"><div class="pic"></div></div>' +
    '<div class="message"></div>' +
    '<div class="name"></div>' +
    '</div>';

ReportSubmitter.MESSAGE_TYPE = {
    ERROR: 1,
    WARNING: 2,
    GOOD: 8,
    NORMAL: 9
};

// A loading image URL.
ReportSubmitter.LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif';

// END Submit Trip Report Code



window.onload = function() {
    window.reportSubmitter = new ReportSubmitter();
};


// parse a date in yyyy-mm-dd format
function parseDate(input) {
    var parts = input.split('-');
    // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
    return new Date(parts[0], parts[1]-1, parts[2]); // Note: months are 0-based
}

// Returns a string of format yyyy-MM-dd
Date.prototype.getShortDate = function() {
    if (this) {
        var dd = this.getDate();
        var mm = this.getMonth() + 1; //January is 0!
        var yyyy = this.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        return (yyyy + '-' + mm + '-' + dd);
    }
    return '';
};

// Removes unwanted elements from array
Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};