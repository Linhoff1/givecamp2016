function initMapController(){
    var mapElement = $('#map');
    var isMobile = screen.width <= 700;
    var mapIsFullscreen = false;

    function updateButtonTextOnEscapeButtonPress(){
        isFullscreen = !!document.fullscreen || !!document.mozFullScreen || !!document.webkitIsFullScreen;
        if (!isFullscreen){
            $('#fullscreen').text('Show Fullscreen');
            mapIsFullscreen = false;
        }
    }

    document.addEventListener('fullscreenchange', updateButtonTextOnEscapeButtonPress, false);
    document.addEventListener('mozfullscreenchange', updateButtonTextOnEscapeButtonPress, false);
    document.addEventListener('webkitfullscreenchange', updateButtonTextOnEscapeButtonPress, false);

    function setMapFullScreen(){
        mapIsFullscreen = true;
        $('#fullscreen').text('Exit Fullscreen');
        var elem = mapElement[0];
         if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen();
        }
    }

    function removeMapFullscreen() {
        mapIsFullscreen = false;
        $('#fullscreen').text('Show Fullscreen');
        if (document.cancelFullScreen) {
          document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
          document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
          document.webkitCancelFullScreen();
        }
        if (isMobile) {
           toggleMap($('#show-photo'));
        }
    }

    function swapMapAndPhotoDisplay(){
        var shownElement = $('#photo-switcher').find(':visible');
        var hiddenElement = $('#photo-switcher').find(':hidden');
        shownElement.hide();
        hiddenElement.show();
        renderMap();
    }

    function swapMapAndPhotoButtons(){
        var buttonContainer = $('.toggle-button-container');
        var activeButton = buttonContainer.find(".btn-info");
        var inactiveButton = buttonContainer.find(".btn-secondary");

        activeButton.removeClass("btn-info");
        activeButton.addClass("btn-secondary");

        inactiveButton.removeClass("btn-secondary");
        inactiveButton.addClass("btn-info");
    }

    function createFullScreenMapButton(){
        var fullscreenButton = $('<div>')
            .prop('id', 'fullscreen')
            .addClass('btn')
            .addClass('btn-secondary')
            .text('Show Fullscreen');
        $('#photo-switcher').append(fullscreenButton);

        fullscreenButton.on('click', function(){
            if (mapIsFullscreen){
                removeMapFullscreen();
            }
            else {
                setMapFullScreen();
            }
            google.maps.event.trigger(map,'resize');
            mapElement.css({'height': '100%', width:'100%'})
        });

        return fullscreenButton;
    }

    function toggleMap(target){
        var currentlySelectedButton = $('.toggle-button-container').find(".btn-info");
        if (!currentlySelectedButton.is(target)){
            swapMapAndPhotoDisplay();
            swapMapAndPhotoButtons();
            if (isMobile){
                setMapFullScreen();
            }
        }
    }

    return {
        //these methods will be public
        toggleMap: toggleMap,
        createFullScreenMapButton: createFullScreenMapButton,
        setMapFullScreen: setMapFullScreen
    };
}