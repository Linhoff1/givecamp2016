
var $reports = $("#reports");

$reports.delegate(".toggle-full-view", "click", function () {
    $(this).prev().toggleClass("block-ellipsis");
    $(this).remove();
});

$reports.delegate(".li-image-gallary", "click", function () {
    var url = $(this).attr("src");
    $(this).parent().parent().prev().children()[0].setAttribute("src", url);
});