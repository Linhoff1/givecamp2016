function initFusionTables(callback) {
    google.load('visualization', '1', {'packages': ['table'], callback: callback});
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var parkId = getParameterByName('parkid');


function fetchParkActivityData(callback) {
    var columns = "'Title', 'Activities'";
    var tableId = "1zKDRftAV2oWIPOAkY8Yd7VQjuySK58UFZIbvaRmd";
    var query = "SELECT " + columns + " FROM " + tableId + " WHERE ParkID = '" + parkId + "'";
    callDB(query, function (error, data) {
        if (error) {
            console.error('fetchParkActivityData error: ' + error);
        }
        return callback(error, data);
    });
}

function fetchParkFeatureData(callback) {
    var columns = "'Park_ID', 'icon_name', 'icon_url'";
    var tableId = "11wmZ1JgjvOk4Q4BL6Kmpe85987tsWK9O8PKFZFLn";
    var query = "SELECT " + columns + " FROM " + tableId + " WHERE Park_ID = '" + parkId + "' GROUP BY " + columns;
    callDB(query, function (error, data) {
        if (error) {
            console.error('fetchParkFeatureData error: ' + error);
        }
        return callback(error, data);
    });
}

function fetchContent(callback) {
    var columns = "'ParkID', 'Latitude', 'Longitude', 'Description', 'geometry', 'Title', 'Subtitle', 'Theme', 'Activities', 'Icon', 'Region', 'WSPF_Thumbnail', 'TemplateName', 'WSPF_Text1', 'WSPF_Text2', 'ParkUrl', 'ParkUrl_Text', 'ParkMapUrl', 'ParkMapUrl_Text', 'FriendsGroupURL', 'FriendsGroupText', 'VirtualTourURL', 'VirtualTourUrl_Text', 'Zoom', 'KML', 'Index', 'YouTube_ID', 'features', 'reserve_url'";
    var tableId = "1zKDRftAV2oWIPOAkY8Yd7VQjuySK58UFZIbvaRmd";
    var query = "SELECT " + columns + " FROM " + tableId + " WHERE ParkID = '" + parkId + "'";
    callDB(query, function (error, data) {
        var contentData;
        if (!error && data.length != 1) {
            error = 'got back ' + data.length + ' rows from request';
        }
        if (error) {
            console.error('fetchContent error: ' + error);
        }
        else {
            contentData = data[0];
        }
        return callback(error, contentData);
    });
}

function callDB(query, callback) {
    var gvizQuery = new google.visualization.Query('https://www.google.com/fusiontables/gvizdata?tq=' + encodeURIComponent(query));
    gvizQuery.send(function (response) {
        var data = null;
        var error = null;
        if (response.getDataTable() == null) {
            error = response.getMessage();
        }
        else {
            data = extractResults(response);
        }
        return callback(error, data);
    });
}

function extractResults(response) {
    var table = response.getDataTable();
    var data = [];
    for (var iRow = 0; iRow < table.getNumberOfRows(); iRow++) {
        var dataRow = {};
        for (var iColumn = 0; iColumn < table.getNumberOfColumns(); iColumn++) {
            var label = table.getColumnLabel(iColumn);
            dataRow[label] = table.getValue(iRow, iColumn);
        }
        data.push(dataRow);
    }
    return data;
}

function populateContent(data) {
    $('#title').text(data.Title);
    $('#description-1').text(data.WSPF_Text1);
    $('#description-2').text(data.WSPF_Text2);
    $('#main-photo').prop('src', data.WSPF_Thumbnail);
    if (data.VirtualTourURL) {
        showButton($('#tour'), data.VirtualTourURL);
    }
    if (data.reserve_url) {
        showButton($('#reservations'), data.reserve_url)
    }

    renderMap = function(){
        var map = new google.maps.Map(document.getElementById('map'), {
            center: { lat: parseFloat(data.Latitude), lng: parseFloat(data.Longitude) }
        });

        var fullscreenButton = MapController.createFullScreenMapButton();
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(fullscreenButton[0]);

        var ctaLayer = new google.maps.KmlLayer({
            url: 'http://wspf.org/maps/kml_files/' + data.KML + '.kml',
            map: map
        });
    };
}

function showButton(button, url) {
    button.parent().show();
    button.prop('href', url);
    button.prop('target', '_blank');
}

function populateActivityIcons(data) {
    var iconWrapper = $('#activity-icon-wrapper');
    for (var i = 0; i < data.length; i++) {
        var url = data[i].icon_url;
        var name = data[i].icon_name;
        if (!isIconExcluded(name)) {
            iconWrapper.append($('<img>', {
                src: url,
                class: 'activity-icon',
                alt: name,
                title: name
            }));
        }
    }
}

function isIconExcluded(iconName) {
    var excludedIcons = {
        'Pay Station': true,
        'Telephone': true,
        'Trailhead': true,
        'Tickets': true,
        'Gate': true,
        'Entrance': true,
        'Office': true,
        'Exit': true
    };
    return excludedIcons[iconName] != undefined;
}
