'use strict';

// Initializes a report submitter
function TripReports() {
    this.currentPage = 0;
    this.pages = [];
    this.unapprovedReports = [];
    this.pageSize = 5;

    this.readParams();
    this.initFirebase();
    this.loadReportsStat();
    this.loadReports();
    this.loadUnapprovedReports();
    this.hookupPageNavigation();
}

TripReports.prototype.initFirebase = function() {
    // Shortcuts to Firebase SDK features.
    this.auth = firebase.auth();
    this.database = firebase.database();
    this.storage = firebase.storage();
    // Initiates Firebase auth and listen to auth state changes.
    // this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

// Sets the URL of the given img element with the URL of the image stored in Firebase Storage.
TripReports.prototype.loadImages = function() {
    var tripReports = this;
    $("[data-gs='1']").each(function() {
        var img = $(this);
        var gsurl = img.attr("data-gsurl");
        img.attr("src", "https://www.google.com/images/spin-32.gif");

        tripReports.storage.refFromURL(gsurl).getMetadata().then(function(metadata) {
            img.attr("src", metadata.downloadURLs[0]);
        });
    });
};

TripReports.prototype.hookupPageNavigation = function() {
    $('#previousPage').click((function() {
        if (this.currentPage > 0) {
            this.currentPage -= 1;
            this.updatePagination();
            this.loadReports();
        }
    }).bind(this));

    $('#nextPage').click((function() {
        if (this.currentPage + 1 < this.totalReports / this.pageSize) {
            this.currentPage += 1;
            this.updatePagination();
            this.loadReports();
        }
    }).bind(this));    
};

TripReports.prototype.updatePagination = function() {
    if (this.currentPage == 0) {
        $("#previousPage").addClass("disabled");
    }
    else {
        $("#previousPage").removeClass("disabled");
    }

    if (this.currentPage + 1 < this.totalReports / this.pageSize) {
        $("#nextPage").removeClass("disabled");
    }
    else {
        $("#nextPage").addClass("disabled");
    }
}

TripReports.prototype.formatRating = function(rating) {
    var str = "";
    for(var i=0; i<5; i++) {
        var starType = "fa ";
        if (i < rating) {
            if (i + 1 > rating) {
                starType += "fa-star-half-o";
            }
            else {
                starType += "fa-star";                
            }
        }
        else {
            starType += "fa-star-o";
        }
        str += "<i class='" + starType + "'></i>";
    }
    return str;
};

TripReports.prototype.createReport = function(report, key, contentApproval) {
    var formatTime = function(time) {
        var t = new Date(time);
        return t.getDate() + "." + (t.getMonth() + 1) + "." + t.getFullYear() % 100;
    };

    var formatReortContent = function(str, images) {
        var result = "";
        var needViewMore = false;
        if (str.length > 200) {
            needViewMore = true;
            result += str.substring(0, 200) + " ...";
        }
        else {
            result = str;
        }

        if (needViewMore || images & images.length > 1) {
            result += '<span class="toggle-full-view">VIEW FULL REPORT</span>';
        }

        return result;
    };

    var str = '<div id="report' + key + '" class="report-entry col-sm-12">';

    // start of left part
    str += '<div class="report-left-col col-sm-9">';

    // start of header
    str += '<div class="report-head"><p>';

    str += "<span class='time'>" + formatTime(report.time) + "</span>";
    if (contentApproval) {
        str += "<span class='separator'> | </span>";
        str += "<span class='park_id'>" + report.park_id.replace(/_/g, " ") + "</span>";
    }
    str += "<span class='separator'> | </span>";
    str += "<span class='username'>" + report.user_name + "</span>";    
    if (report.activities && report.activities.length > 0) {
        str += "<span class='separator'> | </span>";
        str += "<span class='activities'>" + $.makeArray(report.activities).join(", ") + "</span>";
    }
    // end of header
    str += '</p></div>';

    // rating row
    str += "<div class='report-rating'><p>" + this.formatRating(+report.star_rating) + "</p></div>";

    // report details
    var content_toolong = report.trip_report_content.length > 200;
    var content_class = content_toolong ? "block-ellipsis" : ""; 
    str += "<div class='report-detail'><p class='text-detail " + content_class + "'>" + report.trip_report_content + "</p>";
    if (content_toolong) {
        str += '<p class="text-detail toggle-full-view">VIEW FULL REPORT</p>';
    }
    str += "</div>";

    // end of left part
    str += '</div>';

    // image part
    if (report.image_url && report.image_url.length > 0) {
        str += '<div class="report-thumbnail-photo col-sm-3">';
        str += '<img class="img-responsive center-block thumbnail-img" alt="image of the park" data-gs="1" data-gsurl="' + report.image_url + '"/>';
        str += '</div>';
    }

    if (contentApproval) {
        str += '<div id="approval-buttons" class="report-left-col col-sm-9">' +
                    '<button class="approve" id="approve' + key + '">Approve</button>' +
                    '<button class="reject" id="reject' + key + '">Reject</button></div>';
    }

    str += "</div>";
    return str;
};

TripReports.prototype.updateAvgRating = function() {
    if (this.totalReports > 0) {
        var avgRating = this.sumOfRatings / this.totalReports;
        avgRating = avgRating.toFixed(1);
        var str = avgRating;
        str += " " + this.formatRating(avgRating);
        str += " (" + this.totalReports + " trip reports )";
        $('#star-rating').append(str);
    }
};

TripReports.prototype.renderReports = function(reports) {
    var strReports = [];
    var strReportKeys = [];

    for(var i in reports) {
        if (i != "_last_report_key_of_next_page_") {
            strReportKeys.push(i);
            strReports.push(this.createReport(reports[i], i , false));
        }
    }

    // We always try to fetch page size + 1 items, that are sorted from oldest to newest.
    // If we do get page size + 1 items, then the first one (the oldest one) belongs to next page,
    // and we store it inside _last_report_key_of_next_page_
    if (strReports.length > this.pageSize) {
        reports._last_report_key_of_next_page_ = strReportKeys[0];
        strReports = strReports.slice(1);
    }

    // reverse so that we generate the report from newwest to oldest
    strReports.reverse();

    $("#reports").empty();
    $("#reports").append(strReports.join(""));
    this.loadImages();
};

// Loads chat messages history and listens for upcoming ones.
TripReports.prototype.loadReports = function() {
    
    // Reference to the reports of the given part
    this.reportsRef = this.database.ref('approved_posts/' + this.parkid);
    this.reportsRef.off();

    if (this.currentPage < this.pages.length) {
        this.renderReports(this.pages[this.currentPage]);
    }
    else {
        var query = this.reportsRef.orderByKey();
        if (this.currentPage > 0) {
            var lastReportKey = this.pages[this.pages.length - 1]._last_report_key_of_next_page_;
            query = query.endAt(lastReportKey);
        }
        query.limitToLast(this.pageSize+1).once('value').then((function(value) {
            var reports = value.val();
            if (reports) {
                this.pages.push(reports);
                this.renderReports(reports);
            }
            else {
                // No report returned for first page.
                // This means we don't have any report for this park.
                // Show the "be the firt to submit a trip report" 
                if (this.currentPage == 0) {
                    $("#firstreport").removeClass("hide");
                    $("#pagination").addClass("hide");
                }
            }
        }).bind(this));
    }
};

TripReports.prototype.loadReportsStat = function() {
    // Reference to the reports stat of the given part
    this.reportsStatRef = this.database.ref('approved_posts_stat/' + this.parkid);
    this.reportsStatRef.off();

    this.reportsStatRef.once('value').then((function(value) {
        var stat = value.val();
        this.totalReports = stat && stat.total_posts || 0;
        this.sumOfRatings = stat && stat.sum_of_ratings || 0;

        this.updateAvgRating();
        this.updatePagination();
    }).bind(this));
};

TripReports.prototype.updateReportsStat = function(parkid, rating_of_new_post) {
    var reportsStatRef = this.database.ref('approved_posts_stat/' + parkid);
    reportsStatRef.off();

    reportsStatRef.once('value').then((function(value) {
        var stat = value.val();
        if (stat) {
            var totalReports = stat && stat.total_posts || 0;
            var sumOfRatings = stat && stat.sum_of_ratings || 0;

            totalReports++;
            sumOfRatings += +rating_of_new_post;

            this.database.ref('approved_posts_stat/' + parkid).set({
                total_posts: totalReports,
                sum_of_ratings: sumOfRatings
            });
        }
    }).bind(this));
}

TripReports.prototype.loadUnapprovedReports = function () {
    // Reference to the /messages/ database path.
    this.messagesRef = this.database.ref('new_posts');
    // Make sure we remove all previous listeners.
    this.messagesRef.off();

    var pageReports = this.unapprovedReports;

    // Loads the last 12 messages.
    this.messagesRef.limitToLast(12).once('value').then((function(value) {
        var str = "";
        var parks = value.val();
        for(var i in parks) {
            var reports = parks[i];
            for(var j in reports) {
                str += this.createReport(reports[j], j, true);
                pageReports.push({ key: j, report: reports[j]});
            }
        }
        $("#unapprovedReports").append(str);
        this.loadImages();
        this.unapprovedReports = pageReports;
        this.bindApprovalButtons(pageReports);
        // this.addApprovalButtons();
    }).bind(this));
};

TripReports.prototype.bindApprovalButtons = function (reports) {
    var tripReports = this;

    $("[id='approval-buttons']").each(function() {
        var div = $(this);
        // On approve event
        div.find('.approve')[0].addEventListener('click', function(e) {
            var reportId = e.srcElement.id.replace('approve', '');
            var unapproved = reports.find(function(r) { return r.key === reportId });

            if (unapproved && unapproved.key) {
                // Write data to approved bucket in Firebase
                firebase.database().ref('approved_posts/' + unapproved.report.park_id + '/' + unapproved.key).set({
                    activities: unapproved.report.activities || [],
                    image_url: unapproved.report.image_url,
                    park_id : unapproved.report.park_id,
                    report_timestamp: unapproved.report.report_timestamp,
                    star_rating: unapproved.report.star_rating,
                    time: unapproved.report.time,
                    trip_report_content: unapproved.report.trip_report_content,
                    user_email: unapproved.report.user_email,
                    user_id: unapproved.report.user_id,
                    user_name: unapproved.report.user_name
                });

                // Update park stat
                tripReports.updateReportsStat(unapproved.report.park_id, unapproved.report.star_rating);

                // Delete post from unapproved
                firebase.database().ref('new_posts/' + unapproved.report.park_id + '/' + unapproved.key).remove();

                // Remove elements on page that represented that report
                $('#report' + unapproved.key).remove();
            }
        });

        // On reject event
        div.find('.reject')[0].addEventListener('click', function(e) {
            // Locate the report
            var reportId = e.srcElement.id.replace('reject', '');
            var report = reports.find(function(r) { return r.key === reportId });
            // Locate the firebase ref
            if (report && report.key && window.confirm("Are you sure you want to reject this report? It will be deleted!")) {
                var uri = 'new_posts/' + report.report.park_id + '/' + report.key;
                var postRef = firebase.database().ref(uri);
                postRef.off();
                // Nuke the report!
                postRef.remove();

                // Remove elements on page that represented that report
                $('#report' + report.key).remove();
            }
        });
    });
};

TripReports.prototype.getParameterByName = function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};

TripReports.prototype.readParams = function() {
    var parkid = this.getParameterByName("parkid");
    this.parkid = parkid;
    this.setSubmitLink(parkid);
};

TripReports.prototype.setSubmitLink = function (parkid) {
    $("[data-writereport='1']").each(function () {
        var button = $(this);
        button.attr("href", "http://wspf.org/submit-trip-report/" + parkid)
    })
};

window.onload = function() {
    window.tripReports = new TripReports();
};